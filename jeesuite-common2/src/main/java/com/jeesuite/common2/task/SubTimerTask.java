package com.jeesuite.common2.task;

public interface SubTimerTask {

	void doSchedule();

	int periodMillis();
}
